<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'filtres_images_vectorise_description' => 'Des filtres pour transformer vos photos JPG en images vectorielles SVG avec mode artistique.',
	'filtres_images_vectorise_nom' => 'Image Vectorise',
	'filtres_images_vectorise_slogan' => 'Vectoriser des photos avec style !',
);

?>